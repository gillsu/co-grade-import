package at.campusonline.exam.export;

import javax.enterprise.context.ApplicationScoped;
import javax.ws.rs.Consumes;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import java.math.BigInteger;
import java.util.HashMap;
import java.util.Map;

@Path("/ahesn")
@Produces(MediaType.APPLICATION_XML)
@Consumes(MediaType.APPLICATION_XML)
@ApplicationScoped
public class AhesnCachedDataService {

  private final Map<BigInteger, String> cache = new HashMap<>();

  @Path("{version}/{resource-type}")
  public String get(@PathParam("version") String version, @PathParam("resource-type") String resourceType, @QueryParam("matrikelnummer") String matrikelnummer) {
    return cache.get(BigInteger.ONE);
  }
}
