package at.campusonline.exam.export.boundary;

import java.math.BigInteger;
import java.time.LocalDateTime;
import java.util.List;

public class Exam {

  private BigInteger dbPrimaryKeyOfCandidate;
  private BigInteger dbPrimaryKeyOfExam;
  private String registrationNumber;
  private String familyNameOfStudent;
  private String firstNameOfStudent;
  private LocalDateTime dateOfAssessment;
  private String grade;
  private String ectsGrade;
  private String remark;
  private String fileRemark;
  private List<PartExam> partExams;

  public BigInteger getDbPrimaryKeyOfCandidate() {
    return dbPrimaryKeyOfCandidate;
  }

  public void setDbPrimaryKeyOfCandidate(BigInteger dbPrimaryKeyOfCandidate) {
    this.dbPrimaryKeyOfCandidate = dbPrimaryKeyOfCandidate;
  }

  public BigInteger getDbPrimaryKeyOfExam() {
    return dbPrimaryKeyOfExam;
  }

  public void setDbPrimaryKeyOfExam(BigInteger dbPrimaryKeyOfExam) {
    this.dbPrimaryKeyOfExam = dbPrimaryKeyOfExam;
  }

  public String getRegistrationNumber() {
    return registrationNumber;
  }

  public void setRegistrationNumber(String registrationNumber) {
    this.registrationNumber = registrationNumber;
  }

  public String getFamilyNameOfStudent() {
    return familyNameOfStudent;
  }

  public void setFamilyNameOfStudent(String familyNameOfStudent) {
    this.familyNameOfStudent = familyNameOfStudent;
  }

  public String getFirstNameOfStudent() {
    return firstNameOfStudent;
  }

  public void setFirstNameOfStudent(String firstNameOfStudent) {
    this.firstNameOfStudent = firstNameOfStudent;
  }

  public LocalDateTime getDateOfAssessment() {
    return dateOfAssessment;
  }

  public void setDateOfAssessment(LocalDateTime dateOfAssessment) {
    this.dateOfAssessment = dateOfAssessment;
  }

  public String getGrade() {
    return grade;
  }

  public void setGrade(String grade) {
    this.grade = grade;
  }

  public String getEctsGrade() {
    return ectsGrade;
  }

  public void setEctsGrade(String ectsGrade) {
    this.ectsGrade = ectsGrade;
  }

  public String getRemark() {
    return remark;
  }

  public void setRemark(String remark) {
    this.remark = remark;
  }

  public String getFileRemark() {
    return fileRemark;
  }

  public void setFileRemark(String fileRemark) {
    this.fileRemark = fileRemark;
  }

  public List<PartExam> getPartExams() {
    return partExams;
  }

  public void setPartExams(List<PartExam> partExams) {
    this.partExams = partExams;
  }
}
