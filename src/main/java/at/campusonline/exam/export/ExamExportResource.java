package at.campusonline.exam.export;

import at.campusonline.exam.export.boundary.Exam;
import at.campusonline.exam.export.boundary.ExamExport;
import at.campusonline.exam.export.boundary.PartExam;

import javax.annotation.security.RolesAllowed;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import io.quarkus.security.identity.SecurityIdentity;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import org.eclipse.microprofile.jwt.JsonWebToken;
import org.eclipse.microprofile.openapi.annotations.Operation;
import org.eclipse.microprofile.openapi.annotations.enums.SchemaType;
import org.eclipse.microprofile.openapi.annotations.media.Content;
import org.eclipse.microprofile.openapi.annotations.media.Schema;
import org.eclipse.microprofile.openapi.annotations.responses.APIResponse;

@Path("/exams")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
@ApplicationScoped
public class ExamExportResource {

  private final Map<BigInteger, ExamExport> cache = new HashMap<>();

  @Inject
  SecurityIdentity securityIdentity;

  @Inject
  JsonWebToken jwt;

  @GET
  @Path("/{dbPrimaryKeyOfExam}")
  @RolesAllowed("export-exams")
  @APIResponse(
          responseCode = "200",
          description = "exam candidates and partial exams of given exam",
          content = @Content(
                  mediaType = "application/json",
                  schema = @Schema(
                          type = SchemaType.OBJECT,
                          implementation = ExamExport.class)))
  @Operation(summary = "exam candidates and partial exams of given exam",
          description = "Returns all registered candidates including their partial exams to the specified "
                        + "dbPrimaryKeyOfExam")
  public ExamExport getExams(@PathParam("dbPrimaryKeyOfExam") BigInteger dbPrimaryKeyOfExam) {
    // return example data from map if exists otherwise return default example data
    if (cache.containsKey(dbPrimaryKeyOfExam)) {
      return cache.get(dbPrimaryKeyOfExam);
    }

    ExamExport export = new ExamExport();
    export.setExams(new ArrayList<>());
    Exam exam = new Exam();
    export.getExams().add(exam);

    exam.setDbPrimaryKeyOfExam(dbPrimaryKeyOfExam);
    exam.setDbPrimaryKeyOfCandidate(BigInteger.valueOf(1234));
    exam.setRegistrationNumber("1234567");
    exam.setFirstNameOfStudent("Andi");
    exam.setFamilyNameOfStudent("Mustermann");
    exam.setDateOfAssessment(LocalDateTime.now());
    exam.setGrade("2,3");

    exam.setPartExams(new ArrayList<>());
    PartExam partExam = new PartExam();
    exam.getPartExams().add(partExam);

    partExam.setPartExamDate(LocalDate.now());
    partExam.setKey("1. Partial exam");
    partExam.setGrade("3,3");
    partExam.setDbPrimaryKeyOfCandidate(exam.getDbPrimaryKeyOfCandidate());
    partExam.setPoints(BigDecimal.valueOf(123));
    partExam.setWeight(BigDecimal.ONE);

    return export;
  }


  @POST
  @Path("/{dbPrimaryKeyOfExam}")
  @RolesAllowed("export-exams")
  @Operation(
          summary = "Add example data",
          description =
                  "Adds data to the in memory storage. Data is added to a map (Map <dbPrimaryKeyOfExam, examExport>) "
                  + "which can later be retrieved by the GET API."
                  + "\n\nThis API is not part of the specifaction. Its purpose is to help in testing with "
                  + "custom example data during development.")
  public ExamExport addToCache(@PathParam("dbPrimaryKeyOfExam") BigInteger dbPrimaryKeyOfExam, ExamExport examExport) {
    cache.put(dbPrimaryKeyOfExam, examExport);
    return examExport;
  }
}
