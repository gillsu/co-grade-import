package at.campusonline.exam.export;


import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;
import org.eclipse.microprofile.openapi.annotations.OpenAPIDefinition;
import org.eclipse.microprofile.openapi.annotations.info.Contact;
import org.eclipse.microprofile.openapi.annotations.info.Info;

/**
 * @author Gillsu George Thekkekara Puthenparampil <thekkekaraputhenparampil@tugraz.at>
 */
@OpenAPIDefinition(
        info = @Info(
                title = "CO Exam Export API",
                version = "1.0.0",
                contact = @Contact(name = "CampusOnline")))
@ApplicationPath("/api")
public class CoExamExportApplication extends Application {
}
