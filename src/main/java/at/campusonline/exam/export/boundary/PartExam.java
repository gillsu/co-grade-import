package at.campusonline.exam.export.boundary;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.time.LocalDate;

public class PartExam {

  private String key;
  private String grade;
  private String remark;
  private BigInteger dbPrimaryKeyOfCandidate;
  private BigDecimal points;
  private BigDecimal percent;
  private BigDecimal weight;
  private BigDecimal minimumValue;
  private LocalDate partExamDate;

  public String getKey() {
    return key;
  }

  public void setKey(String key) {
    this.key = key;
  }

  public String getGrade() {
    return grade;
  }

  public void setGrade(String grade) {
    this.grade = grade;
  }

  public String getRemark() {
    return remark;
  }

  public void setRemark(String remark) {
    this.remark = remark;
  }

  public BigInteger getDbPrimaryKeyOfCandidate() {
    return dbPrimaryKeyOfCandidate;
  }

  public void setDbPrimaryKeyOfCandidate(BigInteger dbPrimaryKeyOfCandidate) {
    this.dbPrimaryKeyOfCandidate = dbPrimaryKeyOfCandidate;
  }

  public BigDecimal getPoints() {
    return points;
  }

  public void setPoints(BigDecimal points) {
    this.points = points;
  }

  public BigDecimal getPercent() {
    return percent;
  }

  public void setPercent(BigDecimal percent) {
    this.percent = percent;
  }

  public BigDecimal getWeight() {
    return weight;
  }

  public void setWeight(BigDecimal weight) {
    this.weight = weight;
  }

  public BigDecimal getMinimumValue() {
    return minimumValue;
  }

  public void setMinimumValue(BigDecimal minimumValue) {
    this.minimumValue = minimumValue;
  }

  public LocalDate getPartExamDate() {
    return partExamDate;
  }

  public void setPartExamDate(LocalDate partExamDate) {
    this.partExamDate = partExamDate;
  }
}
